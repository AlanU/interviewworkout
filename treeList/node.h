#ifndef NODE_H
#define NODE_H
#include<memory>

class Node
{
public:
    explicit Node(unsigned int defaultValue);
    Node() = delete;
    ~Node();
    void print();
    unsigned int value = 0;
    std::unique_ptr<Node> left = nullptr;
    std::unique_ptr<Node> right = nullptr;
};

#endif // NODE_H
