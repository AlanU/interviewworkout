#include <iostream>
#include <memory>
#include <queue>
#include <list>
#include <vector>
#include "node.h"
#include <random>
using namespace std;

void createTreeBottom (Node * root, unsigned int level,unsigned int currentValue)
{   std::mt19937 rng;
    rng.seed(std::random_device()());
    std::uniform_int_distribution<std::mt19937::result_type> dist6(1,10);
    if(root != nullptr)
    {
        root->left = dist6(rng) < 5? nullptr : make_unique<Node>(currentValue++);//new Node(level+1);//Memory leak
        root->right = dist6(rng) < 5? nullptr :make_unique<Node>(currentValue++);//new Node(level+2);//Memory leak
        if(level != 0)
        {
            --level;
            createTreeBottom(root->left.get(),level,currentValue);
            createTreeBottom(root->right.get(),level,currentValue);
        }
    }
    return;
}

std::unique_ptr<Node> CreateTree(unsigned int maxTreeLevel,unsigned int startValue)
{
   std::unique_ptr<Node> root =  make_unique<Node>(0);
   createTreeBottom(root.get(),maxTreeLevel,startValue);
   return root;

}

void depthFirstPrint(Node * currentNode)
{
    currentNode->print();
    if(currentNode->left != nullptr)
    {
        depthFirstPrint(currentNode->left.get());
    }
    if(currentNode->right != nullptr)
    {
        depthFirstPrint(currentNode->right.get());
    }
    return ;
}

void breathFirstPrint(Node * root)
{
    if(root != nullptr)
    {
        std::queue<Node*> nodeQ;
        std::queue<unsigned int> levelQ;
        nodeQ.push(root);
        levelQ.push(0);
        unsigned currentlevel = 0;
        while(!nodeQ.empty())
        {
            if(currentlevel != levelQ.front())
            {
                std::cout<<std::endl;
            }
            Node * currentNode = nodeQ.front();
            currentlevel = levelQ.front();
            nodeQ.pop();
            levelQ.pop();
            currentNode->print();
            if(currentNode->left != nullptr)
            {
                nodeQ.push(currentNode->left.get());
                levelQ.push(currentlevel+1);
            }

            if(currentNode->right != nullptr)
            {
                nodeQ.push(currentNode->right.get());
                levelQ.push(currentlevel+1);
            }
        }
    }
}

std::vector<std::list<Node*>> breathFirstLinkList(Node * root)
{
    std::vector<std::list<Node*>> listofNodes;
    if(root != nullptr)
    {
        std::queue<Node*> nodeQ;
        std::queue<unsigned int> levelQ;
        nodeQ.push(root);
        levelQ.push(0);
        listofNodes.push_back(std::list<Node*>());
        unsigned currentlevel = 0;
        while(!nodeQ.empty())
        {
            if(currentlevel != levelQ.front())
            {
               listofNodes.push_back(std::list<Node*>());
            }
            Node * currentNode = nodeQ.front();
            currentlevel = levelQ.front();
            nodeQ.pop();
            levelQ.pop();
            listofNodes[currentlevel].push_back(currentNode);
            if(currentNode->left != nullptr)
            {
                nodeQ.push(currentNode->left.get());
                levelQ.push(currentlevel+1);
            }

            if(currentNode->right != nullptr)
            {
                nodeQ.push(currentNode->right.get());
                levelQ.push(currentlevel+1);
            }
        }
    }
    return listofNodes;
}

int main()
{
    std::unique_ptr<Node> root = CreateTree(2,1);
    depthFirstPrint(root.get());
    std::cout<<std::endl;
    breathFirstPrint(root.get());
    std::cout<<std::endl;
    std::vector<std::list<Node*>> nodeList = breathFirstLinkList(root.get());
    for (auto &list : nodeList)
    {
        for(auto & node : list)
        {
            node->print();
        }
        std::cout<<std::endl;
    }

    return 0;
}
