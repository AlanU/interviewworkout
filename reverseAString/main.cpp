#include <iostream>

void reverseString(std::string & stringToReverse)
{
    unsigned int startEnd = static_cast<unsigned int>(stringToReverse.size()/2);
    unsigned int endIndex = static_cast<unsigned int>(stringToReverse.size()-1);
    for (unsigned int index =0 ; index < startEnd; ++index )
    {
        std::swap(stringToReverse[index],stringToReverse[endIndex]);
        --endIndex;
    }

}


int main()
{
    std::string stringToChange = "Hello World";
    std::string testString = stringToChange;
    std::cout << stringToChange << std::endl;
    reverseString(stringToChange);
    std::cout << stringToChange << std::endl;
    std::reverse(testString.begin(),testString.end());
    std::cout << testString << std::endl;
    return 0;
}
